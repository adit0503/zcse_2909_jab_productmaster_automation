function getEntity(entity){
    var data = []
    
    var iter = Entity.findAll(entity)
    while(iter.hasNext()){
        var row = iter.next()
        data.push({
            "model": row.MODEL1,
            "idx": row.idx
        });
    }

    return data
}

try {
    var payload = JSON.parse(Channel.getData());

    var entity = payload["entity"];
    var resp = {
        data: getEntity(entity)
    }

    Channel.setResponseText(JSON.stringify(resp));
    Channel.setResponseCode(200);
} catch (err) {
    Channel.setResponseText(err);
    Channel.setResponseCode(500);
}