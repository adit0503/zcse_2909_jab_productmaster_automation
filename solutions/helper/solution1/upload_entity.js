function addRowInEntity(entity,data) {
    var row = Entity.findByRowkeySuffix(entity, data._rowkeysuffix);
    if(!row){
        Entity.add(entity, data);
    } else if(Number(row.idx) < Number(data.idx)) {
        Entity.update(entity, data)
    }
}

function processIncommingData(entity, data){

    for(var i in data){
        var row = {
            MODEL1: String(data[i]["model"]),
            idx: String(data[i]["idx"]),
            _rowkeysuffix: String(data[i]["model"])
        };
        addRowInEntity(entity, row);
    };
}


try{
    var payload = JSON.parse(Channel.getData());

    var data = payload["data"];
    var entity = payload["entity"];

    processIncommingData(entity, data);
    
    Channel.setResponseText("SUCCESS");
    Channel.setResponseCode(200);
} catch(err) {
    Channel.setResponseText(err);
    Channel.setResponseCode(500);
}