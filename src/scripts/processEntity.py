import glob
import os
import time
import csv
import requests


TEMP = {
    "dev": './src/tmp/JAB_temp.txt',
    "prod": './src/tmp/JAB_CHANNELADVISORFEED_2021-05-12-05.txt'
}
OUTPUT = './src/tmp/entity.csv'         # absolute path to the local entity file


INPUT  = './src/tmp/'               # absolute path to read files from
IDENTIFIER = 'JAB_*.txt'            # regex to get required files
TIME_DIFF = [60, 60*60, 60*60*24]   # (secs) req. difference : depends on automation time [lastMinute, lastHour, lastDay]


ENTITY = {}
NEW_ENTITY = {}


def writeEntityFile(path):

    with open(path, 'a', newline='') as entityfile:
        writer = csv.DictWriter(entityfile, fieldnames=["model", "idx"])
        
        for model, idx in NEW_ENTITY.items():
            writer.writerow({"model": model, "idx": idx})

def readEntityFile(path):
    with open(path, 'r', newline='') as entityfile:
        reader = csv.DictReader(entityfile, fieldnames=["model", "idx"])
        for row in reader:
            ENTITY[row["model"]] = row["idx"]


def processRow(row):
    # TODO: add spliting/merging code
    if row['PARENTORCHILD'] == 'PARENT':
        return row['MODEL']
    return

def checkModel(model):
    return model and model not in ENTITY and model not in NEW_ENTITY

def processFiles(file):
    if not file:
        return

    idx = len(ENTITY)
    with open(file, newline='') as inputfile:
        reader = csv.DictReader(inputfile, delimiter='|')
        for row in reader:
            model = processRow(row)
            if checkModel(model):
                NEW_ENTITY[model] = idx
                idx += 1

def getLatestFiles(path, identifier, diff):
    all_files = glob.glob(path+identifier)

    current_time = time.time()
    li = []
    for file in all_files:
        if current_time - os.path.getctime(file) < diff:
            li.append(file)
    print("getLatestFiles()")
    return li


def temp():
    microserviceUrl = "https://cloud-in.zineone.com/public/api/v1/service/MLDC_ProdCatalog_post?namespace=training_com"
    key = "cloud-in@22827725-7cdb-4432-8f2a-1a07a1509afcZ12040986071883667670"
    chunk = [{'model': '537M', 'idx': 4900}, {'model': '14HA', 'idx': 4901}, {'model': '3UHY', 'idx': 4902}, {'model': '58DP', 'idx': 4903}, {'model': '57ZN', 'idx': 4904}, {'model': '14H9', 'idx': 4905}, {'model': '3UGH', 'idx': 4906}, {'model': '3UH9', 'idx': 4907}, {'model': '581E', 'idx': 4908}, {'model': '583E', 'idx': 4909}, {'model': '58D3', 'idx': 4910}, {'model': '58EJ', 'idx': 4911}, {'model': '14LA', 'idx': 4912}, {'model': '14KD', 'idx': 4913}, {'model': '3UJ1', 'idx': 4914}, {'model': '58FK', 'idx': 4915}, {'model': '3UGP', 'idx': 4916}, {'model': '58EN', 'idx': 4917}, {'model': '58F0', 'idx': 4918}, {'model': '3UGF', 'idx': 4919}, {'model': '58EC', 'idx': 4920}, {'model': '580F', 'idx': 4921}, {'model': '14HC', 'idx': 4922}, {'model': '589R', 'idx': 4923}, {'model': '58D2', 'idx': 4924}, {'model': '58EL', 'idx': 4925}, {'model': '58DK', 'idx': 4926}, {'model': '14ML', 'idx': 4927}, {'model': '58EU', 'idx': 4928}, {'model': '3UGZ', 'idx': 4929}, {'model': '3RYM', 'idx': 4930}]

    resp = requests.post(microserviceUrl, headers={'apikey':key}, json=chunk)
    print(resp, resp.content)

def postEntityData(size=1000):
    microserviceUrl = "https://cloud-in.zineone.com/public/api/v1/service/MLDC_ProdCatalog_post?namespace=training_com"
    key = "cloud-in@22827725-7cdb-4432-8f2a-1a07a1509afcZ12040986071883667670"
    
    list_ENTITY = list(NEW_ENTITY)
    i = 0
    while(i<len(list_ENTITY)):
        chunk = []
        for entity in list_ENTITY[i:i+size]:
            chunk.append({
                "model": entity,
                "idx": NEW_ENTITY[entity]
            })
        resp = requests.post(microserviceUrl, headers={'apikey':key}, json=chunk)
        print(resp, resp.content)
        i += size

def getEntityData():
    microserviceUrl = "https://cloud-in.zineone.com/public/api/v1/service/MLDC_ProdCatalog_get?namespace=training_com"
    key = "cloud-in@3df9e0c8-e447-422c-9f4e-5042fad92433Z12040986071883667670"
    
    resp = requests.get(microserviceUrl, headers={'apikey':key})
    entityData = resp.json()["data"]
    for data in entityData:
        ENTITY[data["model"]] = data["idx"]


if __name__ == "__main__":

    '''Method 1. Using local csv file (only POST)
    '''
    # readEntityFile(OUTPUT)
    # processFiles(TEMP['prod'])
    # writeEntityFile(OUTPUT)

    '''Method 2. Using C3 Entity data (GET & POST)
    '''
    getEntityData()
    processFiles(TEMP['prod'])
    print(len(ENTITY), len(NEW_ENTITY))
    postEntityData()

    # temp()

    # files = getLatestFiles(INPUT, IDENTIFIER, TIME_DIFF[2])
    # for file in files:
    #     readFiles(file)