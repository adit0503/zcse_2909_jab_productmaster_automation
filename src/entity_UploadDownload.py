import csv
import requests


# *************************** UPLOAD *************************** #
def postEntity(entity, microservice, size=1000):
    entity_list = list(entity)
    i = 0
    while(i<len(entity_list)):
        chunk = {
            "data" : [],
            "entity" : _ENTITY_NAME
        }
        for ent in entity_list[i:i+size]:
            chunk["data"].append({
                "p123": entity[ent]["p123"],
                "idx": str(entity[ent]["idx"]),
                "model": ent
            })
        # print(chunk)
        resp = requests.post(microservice["url"], headers={'apikey':microservice["key"]}, json=chunk)
        print(resp, resp.content)
        i += size

def readEntity(entity, path):
    with open(path, 'r', newline='') as entityfile:
        reader = csv.DictReader(entityfile, fieldnames=['model','p123','idx'])
        for row in reader:
            entity[row["model"]] = {
                "idx" : int(row["idx"]),
                "p123" : row["p123"]
            }

def upload(config, path):
    _ENTITY = {}
    readEntity(_ENTITY, path)
    postEntity(_ENTITY, config["POST"])


# ************************* DOWNLOAD ************************** #
def getEntity(entity, microservice):
    resp = requests.get(microservice["url"], headers={'apikey' : microservice["key"]}, json={"entity":_ENTITY_NAME})
    entity_data = resp.json()
    for data in entity_data["data"]:
        entity[data["model"]] = {
            "idx" : int(data["idx"]),
            "p123" : data["p123"]
        }

def writeEntity(entity, path):

    with open(path,'w', newline='') as entityfile:
        writer = csv.DictWriter(entityfile, fieldnames=["model", "p123", "idx"])
        for model, val in entity.items():
            writer.writerow({"model": model, "p123" : val["p123"], "idx": val["idx"]})

def download(config, path):
    _ENTITY = {}
    getEntity(_ENTITY, config["GET"])
    writeEntity(_ENTITY, path)
    

if __name__ == "__main__":

    _ENTITY_NAME = "MLDC_ProdCatalog3"

    _config = {
        "dev" : {
            "POST" : {
                "url" : "https://cloud-in.zineone.com/public/api/v1/service/upload_entity?namespace=training_com",
                "key": "cloud-in@e51a113a-cce0-48a1-afb1-d8eca3bf3ba0Z12040986071883667670"
            },
            "GET" : {
                "url" : "https://cloud-in.zineone.com/public/api/v1/service/download_entity?namespace=training_com",
                "key": "cloud-in@55fdead8-f7b5-411f-8b9f-9b2c62ff3b57Z12040986071883667670"
            }
        },

        "prod" : {
            "url" : "",
            "key" : ""
        }
    }
    
    _path = {
        "read" : "./data/P123_v3.csv",
        "write" : "./data/download.csv"
    }

    _env = "dev"

    # upload(_config[_env], _path["read"])
    download(_config[_env], _path["write"])