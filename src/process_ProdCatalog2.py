import glob
import time
import os
import csv
import requests



def _checkModel(model, entity, newentity):
    return model and model not in entity and model not in newentity

def _getP123(row):
    PC = row['PARENTCATEGORY']
    if PC:
        p = PC.split('>') if '>' in PC else PC.split(' ', 1)
        return p
        
    return ""

def _processRow(row):
    if row['PARENTORCHILD'] == 'PARENT':
        p123 = _getP123(row)
        return row['MODEL'], p123

    return "", ""

def _processFile(entity, newentity, file):
    if not file:
        return

    idx = len(entity)+1
    with open(file, newline='') as inputfile:
        reader = csv.DictReader(inputfile, delimiter='|')
        for row in reader:
            model, p123 = _processRow(row)
            if _checkModel(model, entity, newentity):
                if not p123:
                    print(f'valid model:{model}, invalid p123')
                else:
                    print(f'valid model: {model}, valid p123: {p123}')

                # newentity[model] = idx
                # idx += 1

def _getLatestFiles(path, identifier, diff):
    all_files = glob.glob(path+identifier)

    current_time = time.time()
    li = []
    for file in all_files:
        if current_time - os.path.getctime(file) < diff:
            li.append(file)
    return li

def processEntity(entity, newentity, path):

    files = _getLatestFiles(path["input"], path["identifier"], path["diff"])
    for file in files:
        _processFile(entity, newentity, file)



def postEntity(newentity, microservice, size=1000):
    entity_list = list(newentity)
    i = 0
    while(i<len(entity_list)):
        chunk = {
            "data" : [],
            "entity" : _ENTITY_NAME
        }
        for ent in entity_list[i:i+size]:
            chunk["data"].append({
                "model": ent,
                "idx": str(newentity[ent])
            })
        # print(chunk)
        resp = requests.post(microservice["url"], headers={'apikey':microservice["key"]}, json=chunk)
        print(resp, resp.content)
        i += size

def writeReference():
    pass

def readReference(entity, path):
    with open(path, 'r', newline='') as referencefile:
        reader = csv.DictReader(referencefile, fieldnames=['model','p123','idx'])
        for row in reader:
            entity[row["model"]] = {
                "p123" : row["p123"],
                "idx" : int(row['idx'])
            }

if __name__ == "__main__":

    _ENTITY_NAME = "MLDC_ProdCatalog2"

    _ENTITY = {}
    _NEW_ENTITY = {}

    _config = {
        "dev" : {
            "POST" : {
                "url" : "https://cloud-in.zineone.com/public/api/v1/service/post_MLDC_ProdCatalog2?namespace=training_com",
                "key": "cloud-in@b6b18e76-c5cb-4e22-8a3b-22ad6c3be6ddZ12040986071883667670"
            },
        },

        "prod" : {
            "url" : "",
            "key" : ""
        }
    }

    _path = {
        "INPUT":"./mnt/z1-menswearhouse/JosABank_Catalog/", # ml-india server
        "identifier": "JAB_*.txt",
        "diff": 60*60*24*30, # daily run
        "reference" : './data/p123.csv',

        "input" : './tmp/',
    }

    readReference(_ENTITY, _path["reference"])
    processEntity(_ENTITY, _NEW_ENTITY, _path)

    print(len(_NEW_ENTITY))


    # writeReference()
    # postEntity()