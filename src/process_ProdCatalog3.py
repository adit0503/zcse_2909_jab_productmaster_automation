import glob
import time
import os
import csv
import requests



def _checkModel(model, entity, newentity):
    return model and model not in entity and model not in newentity

def _processRow(row):
    if row['PARENTORCHILD'] == 'PARENT':
        return row['MODEL']
    return

def _processFile(entity, newentity, file):
    if not file:
        return

    idx = len(entity)+1
    with open(file, newline='') as inputfile:
        reader = csv.DictReader(inputfile, delimiter='|')
        for row in reader:
            model = _processRow(row)
            if _checkModel(model, entity, newentity):
                newentity[model] = idx
                idx += 1

def _getLatestFiles(path, identifier, diff):
    all_files = glob.glob(path+identifier)

    current_time = time.time()
    li = []
    for file in all_files:
        if current_time - os.path.getctime(file) < diff:
            li.append(file)
    return li

def processEntity(entity, newentity, path):

    files = _getLatestFiles(path["input"], path["identifier"], path["diff"])
    for file in files:
        _processFile(entity, newentity, file)



def postEntity(newentity, microservice, size=1000):
    entity_list = list(newentity)
    i = 0
    while(i<len(entity_list)):
        chunk = {
            "data" : [],
            "entity" : _ENTITY_NAME
        }
        for ent in entity_list[i:i+size]:
            chunk["data"].append({
                "model": ent,
                "idx": str(newentity[ent])
            })
        # print(chunk)
        resp = requests.post(microservice["url"], headers={'apikey':microservice["key"]}, json=chunk)
        print(resp, resp.content)
        i += size

def getEntity(entity, microservice):
    resp = requests.get(microservice["url"], headers={'apikey' : microservice["key"]}, json={"entity":_ENTITY_NAME})
    entity_data = resp.json()
    for data in entity_data["data"]:
        entity[data["model"]] = int(data["idx"])


if __name__ == "__main__":

    _ENTITY_NAME = "MLDC_ProdCatalog3"

    _ENTITY = {}
    _NEW_ENTITY = {}

    _config = {
        "dev" : {
            "GET" : {
                "url" : "https://cloud-in.zineone.com/public/api/v1/service/get_MLDC_ProdCatalog1?namespace=training_com",
                "key": "cloud-in@5ab2f14e-6c3d-4b9c-a834-5c9be4ba7a95Z12040986071883667670"
            },
            "POST" : {
                "url" : "https://cloud-in.zineone.com/public/api/v1/service/post_MLDC_ProdCatalog1?namespace=training_com",
                "key": "cloud-in@0e78c482-37ff-4ffe-9adc-596e9c2af0d6Z12040986071883667670"
            },
        },

        "prod" : {
            "GET" : {
                "url" : "",
                "key": ""
            },
            "POST" : {
                "url" : "",
                "key": ""
            },
        }
    }

    _path = {
        "input":"./mnt/z1-menswearhouse/JosABank_Catalog/", # ml-india server
        "identifier": "JAB_*.txt",
        "diff": 60*60*24, # daily run
    }

    _ENV = "dev"

    getEntity(_ENTITY, _config[_ENV]["GET"])
    processEntity(_ENTITY, _NEW_ENTITY, _path)
    postEntity(_NEW_ENTITY, _config[_ENV]["POST"])