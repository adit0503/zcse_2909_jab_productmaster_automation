import csv



def writeP123(p123, path):
    with open(path, 'w', newline='') as tempfile:
        writer = csv.DictWriter(tempfile, fieldnames=['model','p123'])
        for model, p in p123.items():
            writer.writerow({'model': model, 'p123': p})


def readP123(p123, path):
    with open(path, 'r', newline='') as tempfile:
        reader = csv.DictReader(tempfile, fieldnames=['MODEL','p123'])
        for row in reader:
            p123[row["MODEL"]] = row['p123']


if __name__ == "__main__":
    
    _P123 = {}

    path = {
        "read" : {
            "dev" : "./data/p123.csv",
            "prod" : "./data/P123.csv"
        },
        "write" : {
            "dev" : "./data/p123_write.csv",
            "prod" : "./data/P123_write.csv"
        }
    }
    
    readP123(_P123, path["read"]["prod"])
    writeP123(_P123, path["write"]["prod"])