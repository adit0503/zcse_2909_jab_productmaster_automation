import csv
import requests


# *************************** UPLOAD *************************** #
def postEntity(entity, microservice, size=1000):
    entity_list = list(entity)
    i = 0
    while(i<len(entity_list)):
        chunk = {
            "data" : [],
            "entity" : _ENTITY_NAME
        }
        for ent in entity_list[i:i+size]:
            chunk["data"].append({
                "idx": str(ent),
                "model": entity[ent]
            })
        # print(chunk)
        resp = requests.post(microservice["url"], headers={'apikey':microservice["key"]}, json=chunk)
        print(resp, resp.content)
        i += size

def readEntity(entity, path):
    with open(path, 'r', newline='') as entityfile:
        reader = csv.DictReader(entityfile, fieldnames=['model','idx'])
        for row in reader:
            entity[int(row["idx"])] = row["model"]

def upload(config, path):
    _ENTITY = {}
    readEntity(_ENTITY, path)
    postEntity(_ENTITY, config["POST"])


# ************************* DOWNLOAD ************************** #
def getEntity(entity, microservice):
    resp = requests.get(microservice["url"], headers={'apikey' : microservice["key"]}, json={"entity":_ENTITY_NAME})
    entityData = resp.json()
    for data in entityData["data"]:
        entity[int(data["idx"])] = data["model"]

def writeEntity(entity, path):

    with open(path,'w', newline='') as entityfile:
        writer = csv.DictWriter(entityfile, fieldnames=["model", "idx"])
        for idx, model in entity.items():
            writer.writerow({"model": model, "idx": idx})

def download(config, path):
    _ENTITY = {}
    getEntity(_ENTITY, config["GET"])
    writeEntity(_ENTITY, path)
    

if __name__ == "__main__":

    _ENTITY_NAME = "temp_MLDC_ProdCatalog_Automation"
    _ENTITY_ATTRIBUTES = ["MODEL1", "idx"]

    _config = {
        "dev" : {
            "POST" : {
                "url" : "https://cloud-in.zineone.com/public/api/v1/service/upload_entity_automation?namespace=training_com",
                "key": "cloud-in@032a21c2-9c6e-4570-89b1-78791e1e6ab5Z12040986071883667670"
            },
            "GET" : {
                "url" : "https://cloud-in.zineone.com/public/api/v1/service/download_entity_automation?namespace=training_com",
                "key": "cloud-in@9a59ae18-ae0c-4241-9366-9dec204c149fZ12040986071883667670"
            }
        },
    }
    
    _path = {
        "read" : {
            "dev" : "./data/entity.csv",
            "prod" : "./data/ENTITY.csv"
        },
        "write" : {
            "dev" : "./data/entity_write.csv",
            "prod" : "./data/ENTITY_write.csv"
        }
    }

    upload(_config["dev"], _path["read"]["dev"])
    # download(_config["train"], _path["write"]["prod"])