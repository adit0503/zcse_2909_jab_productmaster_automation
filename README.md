### Helper Function:
**For Uplaod/Download of Entity data**

        * microservice:
            - upload_entity.js
            - download_entity.js
        * scripts:
            - entity_UploadDownload.py

## Solution #1:
**Current entity (model-idx) : get -> process -> post**

    * microservice:
        - get_MLDC_ProdCatalog1.js
        - post_MLDC_ProdCatalog1.js
    * script:
        - process_ProdCatalog1.py

## Solution #2:
**Local csv file (model-p123-idx) + Current entity (model-idx) : read -> process -> write -> post**

    * microservice:
        - post_MLDC_ProdCatalog2.js
    * script:
        - process_ProdCatalog2.py

## Solution #3:
**New entity (model-p123-idx) : get -> process -> post**

    * microservice:
        - get_MLDC_ProdCatalog3.js
        - post_MLDC_ProdCatalog3.js
    * script:
        - process_ProdCatalog3.py